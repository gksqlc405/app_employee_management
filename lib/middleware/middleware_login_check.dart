
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/pages/page_first.dart';
import 'package:app_employee_management/pages/page_main.dart';
import 'package:app_employee_management/pages/page_member_login.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? token = await TokenLib.getToken();  // null 값일수가 있기때문에 String에 ? 붙힘

    /*
    최초에 앱을 실행하면 token은 null이고
    한번이라도 로그아웃을 했다면 빈문자열임. (로그아웃 시 setToken에서 ''로 삭제해버림)
    token이 없으면 비회원용 메인으로 강제이동
    token이 있으면 회원용 메인으로 강제이동
     */
    if (token == null || token == '') {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMemberLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMain()), (route) => false);
    }
  }
}