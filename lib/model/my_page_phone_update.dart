class MyPagePhoneUpdate {
  String phone;

  MyPagePhoneUpdate(this.phone);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['phone'] = this.phone;

    return data;
}

}