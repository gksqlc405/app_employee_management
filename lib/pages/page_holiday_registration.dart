import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_appbar_popup.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_dropdown.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/model/holiday_registration_request.dart';
import 'package:app_employee_management/model/member_login_response.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageHolidayRegistration extends StatefulWidget {
  const PageHolidayRegistration({Key? key}) : super(key: key);



  @override
  State<PageHolidayRegistration> createState() => _PageHolidayRegistrationState();
}

class _PageHolidayRegistrationState extends State<PageHolidayRegistration> {
  final _formKey = GlobalKey<FormBuilderState>();

  String reason = '';
  String holidayType= '';
  String dateHolidayStart ='';
  String dateHolidayEnd = '';
  double increaseOrDecreaseValue = 0;

  Future<void> _setHoliday(HolidayRegistrationRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMember().setHolidayRegistration(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀 로딩 닫고

      ComponentNotification(
        success: true,
        title: '휴가 신청 성공',
        subTitle: res.msg, //백엔드에서 반환받은 메세지 넣기
      ).call();

      //창 닫으면서 다 처리하고 정상적으로 닫혔다 !!!!! 라고 알려준다
      Navigator.pop(
          context,
          [true]
          // 다이알로그 창 닫기
      // <- 다 처리하고 정상적으로 닫혔다는 의미
      );
    }).catchError((err) {
      BotToast.closeAllLoading();  // 커스텀로딩 닫기

      ComponentNotification(
        success: false, title: '휴가신청 실패', subTitle: '휴가 갯수가 부족합니다',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '휴가신청',
      ),
      body: _buildBody(),backgroundColor: Colors.white,
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          onPressed: () {
            _showSetDialog();

            },

          child: Text(
              '휴가신청',
            style: TextStyle(
              fontFamily: 'naverTitle',
              color: colorMain
            ),
          ),
        ),
      ),

    );
  }
  Widget _buildBody() {
    return SingleChildScrollView(  // column 등 이것저것 넣으면 키보드가 활성화 될때 나올때 에러가 나올수있어 넣어준다
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(5),
          margin: EdgeInsets.all(5),
          child: Column(
            children: [
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'reason',
                decoration: const InputDecoration(
                  labelText: '휴가사유',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                    color: colorMain
                  ),
                  suffixIcon: Text(
                    '2자이상~30자이하   ',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                      color: colorMain,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(30, errorText: formErrorMaxLength(30)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 20,),
              FormBuilderDropdown(
                name: 'holidayType',
                decoration: const InputDecoration(
                  labelText: '휴가종류',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
                ]),
                items: dropdownHolidayType,
              ),
              SizedBox(height: 20,),
              FormBuilderDateTimePicker(
                name: 'dateHolidayStart',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain
                  ),
                  labelText: '휴가시작일',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),

                    onPressed: () {
                      _formKey.currentState!.fields['dateHolidayStart']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),
              SizedBox(height: 20,),
              FormBuilderDateTimePicker(
                name: 'dateHolidayEnd',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: '휴가 종료일',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      _formKey.currentState!.fields['dateHolidayEnd']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'increaseOrDecreaseValue',
                decoration: const InputDecoration(
                  labelText: '휴가갯수',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain
                  ),
                  suffixIcon: Text(
                    '숫자만 입력하세요   ',
                    style: TextStyle(
                      height: 5,
                      wordSpacing: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                        color: colorMain
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([  // compose = 합성, 구성, 창작하다
                  FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
                  FormBuilderValidators.numeric(errorText: formErrorNumeric), // err 숫자만 입력해주세요.
                ]),
                keyboardType: TextInputType.number,
              ),
            ],
          ),
        ),
        
      ),
    );
  }

  void _showSetDialog() {
    showDialog(context: context,
        barrierDismissible: true,  //뒷배경의 touchEvent 가능여
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('휴가 날짜와 갯수가 확실합니까?'), // 제목
            content: const Text('정말 휴가신청을 하시겠습니까?'),  // 부제목
            actions: [
              OutlinedButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      HolidayRegistrationRequest request = HolidayRegistrationRequest(
                        _formKey.currentState!.fields['reason']!.value,
                        _formKey.currentState!.fields['holidayType']!.value,
                        DateFormat('yyyy-MM-dd').format(
                            _formKey.currentState!.fields['dateHolidayStart']!.value),
                        DateFormat('yyyy-MM-dd').format(
                            _formKey.currentState!.fields['dateHolidayEnd']!.value),
                        double.parse(_formKey.currentState!.fields['increaseOrDecreaseValue']!.value),
                      );

                      Navigator.pop(
                        context,
                        [true],
                      );
                      _setHoliday(request);
                    }
                  },
                  child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);  // 취소일땐 삭제하면 안돼기 때문에 pop
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }

}
