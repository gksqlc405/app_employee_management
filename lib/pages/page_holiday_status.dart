import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_count_title.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_holiday_status_item.dart';
import 'package:app_employee_management/components/component_no_contents.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/config/config_holiday_dropdown.dart';
import 'package:app_employee_management/model/holiday_list_result.dart';
import 'package:app_employee_management/model/holiday_status_item.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageHolidayStatus extends StatefulWidget {
   PageHolidayStatus({super.key, required this.year});

  int year;

  @override
  State<PageHolidayStatus> createState() => _PageHolidayStatusState();
}

class _PageHolidayStatusState extends State<PageHolidayStatus> {
  final _scrollController = ScrollController();

  List<HolidayStatusItem> _list = [];

  int _totalItemCount = 0;

  String holidayType= '';
  String dateHoliday = '';
  String approvalStatus = '';
  String dateCreate = '';

  int todayYear = DateTime.now().year;


  @override
  void initState() {
    super.initState();
    _holidayList();
  }


  Future<void> _holidayList({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
    }


      BotToast.showCustomLoading(toastBuilder: (cancelFunc) { // 시작하기전에 미리 띄우고
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });


      await RepoMember()
          .getHolidayList(todayYear)
          .then((res) {
        BotToast.closeAllLoading(); // 성공했을때도 닫고

        setState(() {

          _list = res.list;
          _totalItemCount = res.totalItemCount;
        });
      }).catchError((err) {
        BotToast.closeAllLoading();


        ComponentNotification(
          success: false,
          title: '데이터 로딩 실패',
          subTitle: '$todayYear년도에 신청한 연차가없습니다',
        ).call();
      });



    if (reFresh) {
      _scrollController.animateTo(
          0, duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    }
  }


  @override
  Widget build(BuildContext context) {

      return Scaffold(
        appBar: ComponentAppbarNormal(
          title: '휴가신청 현황',
        ),
        body: ListView(
          controller: _scrollController,
          children: [
            SizedBox(
              height: 30,
            ),
            ComponentCountTitle(icon: Icons.list,
                count: _totalItemCount,
                unitName: '건',
                itemName: '신청내역'),
            Divider(
              thickness: 2,
              color: colorMain,
            ),
            SizedBox(
              height: 20,
            ),
            _buildBody(),
            SizedBox(
              height: 30,
            ),
            _buildList(),
          ],
        ),
      );

  }


  Widget _buildList() {
      if (_totalItemCount != 0) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) =>
                  ComponentHolidayStatusItem(
                    holidayStatusItem: _list[index],

                  ),
            ),
          ],
        );
      } else {
        return SizedBox(
          height: MediaQuery
              .of(context)
              .size
              .height - 45,
          child: const ComponentNoContents(
              imageName: 'line.png', msg: '데이터가 없습니다.'),
        );
      }
  }

  Widget _buildBody() {
      return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            FormBuilderDropdown<int>(
              name: 'todayYear',
              decoration: const InputDecoration(
                labelText: '휴가 신청 년도',
                labelStyle: TextStyle(
                    fontFamily: 'maple',
                    color: colorMain
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  borderSide: BorderSide(color: colorMain),
                ),
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: formErrorRequired),
                //err 이 값은 필수입니다
              ]),
              items: dropdownHolidayUse,
              initialValue: todayYear,
              onChanged: (val) {
                setState(() {
                  todayYear = val!;
                  _holidayList();
                });
              },
            ),

          ],
        ),
      );
  }
}

