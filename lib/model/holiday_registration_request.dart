
class HolidayRegistrationRequest {
   String reason;
   String holidayType;
   String dateHolidayStart;
   String dateHolidayEnd;
   double increaseOrDecreaseValue;

   HolidayRegistrationRequest(
       this.reason,
       this.holidayType,
       this.dateHolidayStart,
       this.dateHolidayEnd,
       this.increaseOrDecreaseValue
       );

   Map<String, dynamic> toJson() {
     Map<String, dynamic> data = Map<String, dynamic>();

     data['reason'] = this.reason;
     data['holidayType'] = this.holidayType;
     data['dateHolidayStart'] = this.dateHolidayStart;
     data['dateHolidayEnd'] = this.dateHolidayEnd;
     data['increaseOrDecreaseValue']  = this.increaseOrDecreaseValue;

     return data;
   }
}