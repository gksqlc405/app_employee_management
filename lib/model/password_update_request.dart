class PasswordUpdateRequest {
  String password;
  String newPassword;
  String checkPassword;

  PasswordUpdateRequest(this.password, this.newPassword, this.checkPassword);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['password'] = this.password;
    data['newPassword'] = this.newPassword;
    data['checkPassword'] = this.checkPassword;

    return data;
  }
}