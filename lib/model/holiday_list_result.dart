
import 'package:app_employee_management/model/holiday_status_item.dart';

class HolidayLisResult {
  List<HolidayStatusItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  HolidayLisResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory HolidayLisResult.fromJson(Map<String, dynamic> json) {
    return HolidayLisResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => HolidayStatusItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}