import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

int todayYear = DateTime.now().year;

final List<DropdownMenuItem<int>> dropdownHolidayUse = [
DropdownMenuItem(value: todayYear - 5, child: Text('${todayYear - 5}년')),
DropdownMenuItem(value: todayYear - 4, child: Text('${todayYear - 4}년')),
DropdownMenuItem(value: todayYear - 3, child: Text('${todayYear - 3}년')),
DropdownMenuItem(value: todayYear - 2, child: Text('${todayYear - 2}년')),
  DropdownMenuItem(value: todayYear - 1, child: Text('${todayYear - 1}년')),
  DropdownMenuItem(value: todayYear, child: Text('$todayYear년')),
  DropdownMenuItem(value: todayYear + 1, child: Text('${todayYear + 1}년')),
  DropdownMenuItem(value: todayYear + 2, child: Text('${todayYear + 2}년')),
  DropdownMenuItem(value: todayYear + 3, child: Text('${todayYear + 3}년')),
  DropdownMenuItem(value: todayYear + 4, child: Text('${todayYear + 4}년')),
  DropdownMenuItem(value: todayYear + 5, child: Text('${todayYear + 5}년')),
];


