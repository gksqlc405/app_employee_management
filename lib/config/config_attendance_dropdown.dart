import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

int january = DateTime.january;
int february = DateTime.february;
int march = DateTime.march;
int april = DateTime.april;
int may = DateTime.may;
int june = DateTime.june;
int july = DateTime.july;
int august = DateTime.august;
int september = DateTime.september;
int october = DateTime.october;
int november = DateTime.november;
int december = DateTime.december;


final List<DropdownMenuItem<int>> dropdownAttendance = [
  DropdownMenuItem(value: january , child: Text('${january}월')),
  DropdownMenuItem(value: february , child: Text('${february}월')),
  DropdownMenuItem(value: march , child: Text('${march}월')),
  DropdownMenuItem(value: april, child: Text('${april}월')),
  DropdownMenuItem(value: may, child: Text('${may}월')),
  DropdownMenuItem(value: june, child: Text('$june월')),
  DropdownMenuItem(value: july, child: Text('${july}월')),
  DropdownMenuItem(value: august, child: Text('${august}월')),
  DropdownMenuItem(value: september, child: Text('${september}월')),
  DropdownMenuItem(value: october, child: Text('${october}월')),
  DropdownMenuItem(value: november, child: Text('${november}월')),
  DropdownMenuItem(value: december, child: Text('${december}월')),
];
