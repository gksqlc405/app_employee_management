import 'package:app_employee_management/components/component_appbar_actions.dart';
import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/repository/repo_attendance.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageAttendanceStatus extends StatefulWidget {
  const PageAttendanceStatus({Key? key}) : super(key: key);

  @override
  State<PageAttendanceStatus> createState() => _PageAttendanceStatusState();
}

class _PageAttendanceStatusState extends State<PageAttendanceStatus> {
  String _attendanceStatusName = '';
  String _attendanceStatus = '';
  final _formKey = GlobalKey<FormBuilderState>();
  @override
  void initState() {
    super.initState();
    _getData();
  }

  Future<void> _getData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().getData().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _attendanceStatusName = res.date.attendanceStatusName;
        _attendanceStatus = res.date.attendanceStatus;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      print(err);
    });
  }


  Future<void> _putData(String attendanceStatus) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().putStatus(attendanceStatus).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _attendanceStatusName = res.date.attendanceStatusName;
        _attendanceStatus = res.date.attendanceStatus;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      print(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '근태관리',
      ),
      body: Center(
        child:
        Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/leave.jpeg',
              width: 150,
                height: 150,
              ),
              Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    _buildBody(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
    );
  }


  Widget _buildBody() {
    return SingleChildScrollView(
      child: Center(
        child:
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Text('현재 상태 : $_attendanceStatusName'),
            if (_attendanceStatus == 'NONE')
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(colorMain),
                  minimumSize: MaterialStateProperty.all(Size(150, 150)),
                ),
                onPressed: () {
                  _showAttendance();

                },
                child: Text('출근',
                  style: TextStyle(
                    fontFamily: 'maple',
                    fontSize: 20,
                    color: Colors.white
                  ),
                ),
              ),
            if (_attendanceStatus == 'ATTENDANCE')
              ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(colorMain),
                    minimumSize: MaterialStateProperty.all(Size(150, 150)),
                  ),
                  onPressed: () {
                  _showEarlyLeave();
                  },
                  child: Text('조퇴',
                    style: TextStyle(
                      fontFamily: 'maple',
                      fontSize: 20,
                      color: Colors.white
                    ),
                  ),
              ),
            SizedBox(height: 20,),
            if (_attendanceStatus == 'ATTENDANCE')
              ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(colorMain),
                    minimumSize: MaterialStateProperty.all(Size(150, 150)),
                  ),
                  onPressed: () {
                   _showLeave();
                  },
                  child: Text('퇴근',
                    style: TextStyle(
                      fontFamily: 'maple',
                      fontSize: 20,
                      color: Colors.white
                    ),
                  ),
              ),
            if (_attendanceStatus == 'LEAVE_EARLY' || _attendanceStatus == 'LEAVE_WORK')
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: colorMain,
                      blurRadius: 10.0,
                      spreadRadius: 5.0,
                    ),
                  ],
                  border: Border.all(
                    color: Colors.white,
                    style: BorderStyle.solid,
                    width: 5,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text('${_attendanceStatus.replaceAll('LEAVE_WORK', '퇴근').replaceAll('LEAVE_EARLY', '조퇴')}  처리가 완료되었습니다!!\n\n 오늘 하루도 고생하셨습니다^^',
                  style: TextStyle(
                      fontFamily: 'naverBold',
                      fontSize: 20
                  ),
                ),
              ),

            SizedBox(height: 30,),
            if (_attendanceStatus == 'LEAVE_EARLY' || _attendanceStatus == 'LEAVE_WORK')
            Image.asset('assets/moveimage.gif',
            width: 200,
              height: 200,
            ),
          ],
        ),
      ),
    );
  }


  void _showAttendance() {
    showDialog(context: context,
        barrierDismissible: true,  //뒷배경의 touchEvent 가능여
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('출근 하시겠습니까?',

              style: TextStyle(
              fontFamily: 'naverBold'
            ),
            ),
            content: const Text('확인을 누르시면 변경할수 없습니다'),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    _putData('ATTENDANCE');
                    Navigator.pop
                      (context);
                  },
                  child: const Text('확인')

              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }

  void _showEarlyLeave() {
    showDialog(context: context,
        barrierDismissible: true,  //뒷배경의 touchEvent 가능여
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('조퇴 하시겠습니까?',

              style: TextStyle(
                  fontFamily: 'naverBold'
              ),
            ),
            content: const Text('확인을 누르시면 변경할수 없습니다'),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    _putData('LEAVE_EARLY');
                    Navigator.pop
                      (context);
                  },
                  child: const Text('확인')

              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }

  void _showLeave() {
    showDialog(context: context,
        barrierDismissible: true,  //뒷배경의 touchEvent 가능여
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('퇴근 하시겠습니까?',

              style: TextStyle(
                  fontFamily: 'naverBold'
              ),
            ),
            content: const Text('확인을 누르시면 변경할수 없습니다'),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    _putData('LEAVE_WORK');
                    Navigator.pop
                      (context);
                  },
                  child: const Text('확인')

              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }
}

