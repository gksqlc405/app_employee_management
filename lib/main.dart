import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/login_check.dart';
import 'package:app_employee_management/pages/page_attendance.dart';
import 'package:app_employee_management/pages/page_attendance_search.dart';
import 'package:app_employee_management/pages/page_first.dart';
import 'package:app_employee_management/pages/page_main.dart';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      theme: ThemeData(

        primarySwatch: Colors.grey,
      ),
      home: const LoginCheck()
    );
  }
}
