

import 'package:app_employee_management/components/component_appbar_actions.dart';
import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/pages/page_attendance_count.dart';
import 'package:app_employee_management/pages/page_attendance_date.dart';
import 'package:app_employee_management/pages/page_attendance_search.dart';
import 'package:app_employee_management/pages/page_attendance_status.dart';
import 'package:app_employee_management/repository/repo_attendance.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PageAttendance extends StatefulWidget {
  const PageAttendance({Key? key}) : super(key: key);

  @override
  State<PageAttendance> createState() => _PageAttendanceState();
}

class _PageAttendanceState extends State<PageAttendance> {

  String dateStart= '';
  String dateEnd = '';

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '근태관리',
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: [
            Image.asset('assets/line.png',
            width: 100,
              height: 100,
            ),
            // SizedBox(height: 100,),
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 20,),
                  ElevatedButton(
                    style: ButtonStyle(
                      fixedSize: MaterialStateProperty.all(Size.fromWidth(30)),
                      minimumSize: MaterialStateProperty.all(Size.fromHeight(60)),
                      backgroundColor: MaterialStateProperty.all(colorMain),
                        elevation: MaterialStateProperty.all(7)
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendanceStatus())
                      );
                    },
                    child: Text('출퇴근 등록',
                      style: TextStyle(
                        fontFamily: 'naverBold',
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      fixedSize: MaterialStateProperty.all(Size.fromWidth(30)),
                      minimumSize: MaterialStateProperty.all(Size.fromHeight(60)),
                      backgroundColor: MaterialStateProperty.all(colorMain),
                      elevation: MaterialStateProperty.all(7)
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendanceDate())
                      );
                    },
                    child: Text('출퇴근 날짜 확인',
                      style: TextStyle(
                        fontFamily: 'naverBold',
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                        fixedSize: MaterialStateProperty.all(Size.fromWidth(30)),
                        minimumSize: MaterialStateProperty.all(Size.fromHeight(60)),
                        backgroundColor: MaterialStateProperty.all(colorMain),
                        elevation: MaterialStateProperty.all(7)
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendanceCount())
                      );
                    },
                    child: Text('근무일수 확인',
                      style: TextStyle(
                        fontFamily: 'naverBold',
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  Image.asset('assets/fight.jpeg',
                  width: 100,
                    height: 100,
                  ),
                ],
              ),

            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
}
