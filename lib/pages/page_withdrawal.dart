import 'package:app_employee_management/components/component_appbar_popup.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/model/user_withdrawal_request.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageWithdrawal extends StatefulWidget {
  const PageWithdrawal({Key? key}) : super(key: key);

  @override
  State<PageWithdrawal> createState() => _PageWithdrawalState();
}

class _PageWithdrawalState extends State<PageWithdrawal> {
  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }


  final _formKey = GlobalKey<FormBuilderState>();

  String password = '';
  String checkPassword = '';

  Future<void> _delMember(UserWithdrawalRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMember().delMember(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀 로딩 닫고

      ComponentNotification(
        success: true,
        title: '회원탈퇴 완료',
        subTitle: res.msg, //백엔드에서 반환받은 메세지 넣기
      ).call();

      //창 닫으면서 다 처리하고 정상적으로 닫혔다 !!!!! 라고 알려준다
      Navigator.pop(
          context,
          [true, _logout(context)]
        // 다이알로그 창 닫기
        // <- 다 처리하고 정상적으로 닫혔다는 의미
      );
    }).catchError((err) {
      BotToast.closeAllLoading();  // 커스텀로딩 닫기

      ComponentNotification(
        success: false, title: '회원탈퇴 실패', subTitle: '비밀번호를 다시 확인해 주십시오',
      ).call();
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '회원 탈퇴',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          onPressed: () {
            _showDelDialog();

          },

          child: Text(
              '탈퇴하기',
            style: TextStyle(
              fontFamily: 'naverTitel',
              color: Colors.red,
              fontSize: 15
            ),
          ),
        ),
      ),

    );
  }


  @override
  Widget _buildBody() {
    return SingleChildScrollView(  // column 등 이것저것 넣으면 키보드가 활성화 될때 나올때 에러가 나올수있어 넣어준다
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'password',
                decoration: const InputDecoration(
                  labelText: '비밀번호',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain
                  ),
                  suffixIcon: Text(
                    'password',
                    style: TextStyle(
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                      color: colorMain,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                obscureText: true,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(
                height: 30,
              ),
              FormBuilderTextField(
                name: 'checkPassword',
                decoration: const InputDecoration(
                  labelText: '비밀번호 재확인',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                    color: colorMain
                  ),
                  suffixIcon: Text(
                    'checkPassword',
                    style: TextStyle(
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                      color: colorMain,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                obscureText: true,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              Image.asset('assets/bye.gif',
                width: 450,
                height: 350,
              ),

            ],
          ),
        ),

      ),
    );
  }






  void _showDelDialog() {
    showDialog(context: context,
        barrierDismissible: true,  //뒷배경의 touchEvent 가능여
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('정말 탈퇴하시겠습니까??'), // 제목
            actions: [
              OutlinedButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      UserWithdrawalRequest request = UserWithdrawalRequest(
                        _formKey.currentState!.fields['password']!.value,
                        _formKey.currentState!.fields['checkPassword']!.value,

                      );
                      Navigator.pop(
                        context,
                        [true],
                      );
                      _delMember(request);
                    }
                  },
                  child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }
}