class UserWithdrawalRequest {
  String password;
  String checkPassword;

  UserWithdrawalRequest(this.password, this.checkPassword);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['password'] = this.password;
    data['checkPassword'] = this.checkPassword;

    return data;
  }
}