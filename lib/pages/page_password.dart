import 'package:app_employee_management/components/component_appbar_popup.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/model/password_update_request.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PagePassword extends StatefulWidget {
  const PagePassword({Key? key}) : super(key: key);

  @override
  State<PagePassword> createState() => _PagePasswordState();
}

class _PagePasswordState extends State<PagePassword> {
  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }


  final _formKey = GlobalKey<FormBuilderState>();

  String password = '';
  String checkPassword = '';
  String newPassword = '';

  Future<void> _putPassword(PasswordUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMember().putPassword(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀 로딩 닫고

      ComponentNotification(
        success: true,
        title: '비밀번호 변경완료',
        subTitle: res.msg, //백엔드에서 반환받은 메세지 넣기
      ).call();

      //창 닫으면서 다 처리하고 정상적으로 닫혔다 !!!!! 라고 알려준다
      Navigator.pop(
          context,
          [true, _logout(context)]
        // 다이알로그 창 닫기
        // <- 다 처리하고 정상적으로 닫혔다는 의미
      );
    }).catchError((err) {
      BotToast.closeAllLoading();  // 커스텀로딩 닫기

      ComponentNotification(
        success: false, title: '비밀번호변경 실패', subTitle: '비밀번호를 다시 확인해 주십시오',
      ).call();
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '비밀번호 변경',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          onPressed: () {
            _showPutDialog();

          },

          child: Text(
              '비밀번호 변경하기',
            style: TextStyle(
              fontFamily: 'naverTitle',
                color: colorMain
            ),
          ),
        ),
      ),

    );
  }


  @override
  Widget _buildBody() {
    return SingleChildScrollView(  // column 등 이것저것 넣으면 키보드가 활성화 될때 나올때 에러가 나올수있어 넣어준다
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'password',
                decoration: const InputDecoration(
                  labelText: '비밀번호',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                    color: colorMain
                  ),
                  suffixIcon: Text(
                    'password',
                    style: TextStyle(
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                        color: colorMain
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                obscureText: true,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(
                height: 30,
              ),
              FormBuilderTextField(
                name: 'newPassword',
                decoration: const InputDecoration(
                  labelText: '새로운비밀번호 재확인',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain
                  ),
                  suffixIcon: Text(
                    'newPassword',
                    style: TextStyle(
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                        color: colorMain
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                obscureText: true,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 5,),
              FormBuilderTextField(
                name: 'checkPassword',
                decoration: const InputDecoration(
                  labelText: '새로운비밀번호 재확인',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain
                  ),
                  suffixIcon: Text(
                    'checkPassword',
                    style: TextStyle(
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                        color: colorMain
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                obscureText: true,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
            ],
          ),
        ),

      ),
    );
  }






  void _showPutDialog() {
    showDialog(context: context,
        barrierDismissible: true,  //뒷배경의 touchEvent 가능여
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('비밀번호를 변경하시겠습니까?'), // 제목
            actions: [
              OutlinedButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      PasswordUpdateRequest request = PasswordUpdateRequest(
                        _formKey.currentState!.fields['password']!.value,
                        _formKey.currentState!.fields['newPassword']!.value,
                        _formKey.currentState!.fields['checkPassword']!.value,

                      );
                      Navigator.pop(
                        context,
                        [true],
                      );
                      _putPassword(request);
                    }
                  },
                  child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }
}