import 'package:app_employee_management/components/component_appbar_popup.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/model/my_page_phone_update.dart';
import 'package:app_employee_management/pages/page_my.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageMyPhoneUpdate extends StatefulWidget {
  const PageMyPhoneUpdate({Key? key}) : super(key: key);

  @override
  State<PageMyPhoneUpdate> createState() => _PageMyPhoneUpdateState();
}

class _PageMyPhoneUpdateState extends State<PageMyPhoneUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();

  String phone = '';


  Future<void> _setPhone(MyPagePhoneUpdate myPagePhoneUpdate) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMember().putPhoneUpdate(myPagePhoneUpdate).then((res) {
      BotToast.closeAllLoading(); // 커스텀 로딩 닫고

      ComponentNotification(
        success: true,
        title: '전화번호 수정 완료',
        subTitle: res.msg, //백엔드에서 반환받은 메세지 넣기
      ).call();

      //창 닫으면서 다 처리하고 정상적으로 닫혔다 !!!!! 라고 알려준다
      Navigator.pop(
          context,
          [true]
        // 다이알로그 창 닫기
        // <- 다 처리하고 정상적으로 닫혔다는 의미
      );
    }).catchError((err) {
      BotToast.closeAllLoading();  // 커스텀로딩 닫기

      ComponentNotification(
        success: false, title: '전화번호 수정실패', subTitle: '다시확인해 주십시오',
      ).call();
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: 'MyPage',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          onPressed: () {
            _showSetDialog();

          },

          child: Text(
              '전화번호 수정',
            style: TextStyle(
              fontFamily: 'naverTitle',
                color: colorMain
            ),
          ),
        ),
      ),

    );
  }


  @override
  Widget _buildBody() {
    return SingleChildScrollView(  // column 등 이것저것 넣으면 키보드가 활성화 될때 나올때 에러가 나올수있어 넣어준다
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(15),
          margin: EdgeInsets.all(15),
          child: Column(
            children: [
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'phone',
                decoration: const InputDecoration(
                  labelText: '전화번호',
                  hintText: '  - 는 필수입니다',
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain

                  ),
                  suffixIcon: Text(
                    '010-xxxx-xxxx',
                    style: TextStyle(
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 10,
                        color: colorMain

                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(13, errorText: formErrorMinLength(13)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(13, errorText: formErrorMaxLength(13)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.number,
              ),
            ],
          ),
        ),

      ),
    );
}






void _showSetDialog() {
  showDialog(context: context,
      barrierDismissible: true,  //뒷배경의 touchEvent 가능여
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('정말 이전화번호로 바꾸시겠습니까?'), // 제목
          actions: [
            OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    MyPagePhoneUpdate myPagePhoneUpdate = MyPagePhoneUpdate(
                      _formKey.currentState!.fields['phone']!.value,
                    );
                    Navigator.pop(
                      context,
                      [true],
                    );
                    _setPhone(myPagePhoneUpdate);
                  }
                },
                child: const Text('확인')
            ),
            OutlinedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('취소')
            ),
          ],
        );
      }
  );
}
}