import 'package:app_employee_management/model/member_user_my_pageItem.dart';

class MemberResult {
  bool isSuccess;
  int code;
  String msg;
  UserMyPageItem date;

  MemberResult(this.isSuccess, this.code, this.msg, this.date);

  factory MemberResult.fromJson(Map<String, dynamic> json) {
    return MemberResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      UserMyPageItem.fromJson(json['date']),
    );
  }
}