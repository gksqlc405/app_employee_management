import 'dart:ffi';

import 'package:app_employee_management/config/config_api.dart';
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/model/attendance_count.dart';
import 'package:app_employee_management/model/attendance_list_result.dart';
import 'package:app_employee_management/model/attendance_count_response.dart';
import 'package:app_employee_management/model/attendance_result.dart';

import 'package:dio/dio.dart';

class RepoAttendance {

  Future<AttendanceCountResponse> getAttendanceCount(int year, int month) async {
    const String baseUrl = '$apiUrl/attendance/attendance/{memberId}/year/{year}/month/{month}';

    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!).replaceAll('{year}', year.toString()).replaceAll('{month}', month.toString()),
      options: Options(
        followRedirects: false
      )
    );

    return AttendanceCountResponse.fromJson(response.data);
  }

  Future<AttendanceListResult> getList({String dateStart = '' , String dateEnd = ''}) async {
    const String baseUrl = '$apiUrl/attendance/search/{searchId}';
    Map<String, dynamic> params = {};
     params['dateStart'] = dateStart;
     params['dateEnd'] = dateEnd;


    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{searchId}', token!).replaceAll(dateStart,dateStart.toString()).replaceAll(dateEnd, dateEnd.toString()),
        queryParameters: params,
        options: Options(
            followRedirects: false,
        )
    );

    return AttendanceListResult.fromJson(response.data);
  }

  Future<AttendanceResult> getData() async {
    const String baseUrl = '$apiUrl/attendance/status/member-id/{memberId}';

    Dio dio =Dio();
      String? token = await TokenLib.getToken();



    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!),
      options: Options(
        followRedirects: false
      )
    );

    return AttendanceResult.fromJson(response.data);
  }

  Future<AttendanceResult> putStatus(String attendanceStatus) async {
    const String baseUrl = '$apiUrl/attendance/status/{attendanceStatus}/member-id/{memberId}';

    Dio dio = Dio();
      String? token = await TokenLib.getToken();

    final response = await dio.put(
      baseUrl.replaceAll('{attendanceStatus}', attendanceStatus).replaceAll('{memberId}', token!),
      options: Options(
        followRedirects: false
      )
    );

    return AttendanceResult.fromJson(response.data);
  }


}