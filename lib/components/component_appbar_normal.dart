import 'package:app_employee_management/config/config_color.dart';
import 'package:flutter/material.dart';

class ComponentAppbarNormal extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarNormal({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      centerTitle: true,  // 앱바 타이틀 중앙에 할거니 ?
      automaticallyImplyLeading: true, // 타이틀(글자) 앞에 자동으로 뒤로가기버튼 같은거 달리는거 허용하겠니?
      title: Text(title,
        style: TextStyle(
          color: colorMain,
            fontFamily: 'naverTitle',

        ),

      ),
      elevation: 5,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(45);
}
