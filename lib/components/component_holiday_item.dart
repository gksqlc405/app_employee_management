
import 'package:app_employee_management/model/holiday_item.dart';
import 'package:flutter/material.dart';

class ComponentHolidayItem extends StatelessWidget {
  const ComponentHolidayItem({super.key, required this.holidayUseItem, required this.callback});

  final HolidayUseItem holidayUseItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
              spreadRadius: 3.0,
            ),
          ],
          border: Border.all(
            color: Colors.blueAccent,
              style: BorderStyle.solid,
              width: 5,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Text('총 휴가일수가 ${holidayUseItem.countTotal} 개 입니다.',
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 15,
              ),
            ),
            Text('사용 휴가일수가 ${holidayUseItem.countUse} 개 입니다',
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 15,
              ),
            ),
            Text('휴가 잔여일수가 : ${holidayUseItem.countRemaining} 개 남았습니다',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            ),
          ],
        ),
      ),
    );
  }
}
