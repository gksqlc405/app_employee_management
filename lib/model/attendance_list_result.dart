

import 'package:app_employee_management/model/attendance_item.dart';

class AttendanceListResult {
  List<AttendanceItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  AttendanceListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory AttendanceListResult.fromJson(Map<String, dynamic> json) {
    return AttendanceListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => AttendanceItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}