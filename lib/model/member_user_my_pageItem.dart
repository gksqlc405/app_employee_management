class UserMyPageItem {
  String name;
  String phone;
  String department;
  String position;

  UserMyPageItem(this.name, this.phone, this.department, this.position);

  factory UserMyPageItem.fromJson(Map<String, dynamic> json) {
    return UserMyPageItem(
      json['name'],
      json['phone'],
      json['department'],
      json['position'],
    );
  }
}