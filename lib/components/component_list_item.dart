

import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/model/attendance_item.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.item,

  });

  final AttendanceItem item;



  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: colorMain,
              blurRadius: 1.0,
              spreadRadius: 1.0,
            ),
          ],
          border: Border.all(
            color: Colors.white,
            style: BorderStyle.solid,
            width: 5,
          ),
          borderRadius: BorderRadius.circular(60.0),
        ),
        child: Column(
          children: [
            Text(item.attendanceStatus,
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 15,
                color: Colors.black,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(item.dateWork,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('출근 : ${item.timeStart}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: colorMain,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('조퇴 : ${item.timeLeaveEarly}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: colorMain,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('퇴근 : ${item.timeLeave}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: colorMain,
              ),
            ),
          ],
        ),
      ),
    );
  }
}


//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: callback,
//       child: Container(
//         padding: const EdgeInsets.all(10),
//         margin: const EdgeInsets.only(bottom: 10),
//         decoration: BoxDecoration(
//           color: Colors.white,
//           border: Border.all(color: Colors.pink),
//         ),
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           children: [
//             Text('${item.attendanceStatus == null ? '출근기록이 없습니다' : item.attendanceStatus}',
//               style: TextStyle(
//                 fontFamily: 'maple',
//                 fontSize: 18,
//               ),
//             ),
//             Text('${item.dateWork == null ? '출근 날짜가 없습니다' : item.dateWork}',
//               style: TextStyle(
//                 fontWeight: FontWeight.bold,
//               ),
//             ),
//
//           ],
//         ),
//       ),
//     );
//   }
// }
//
