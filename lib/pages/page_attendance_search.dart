import 'package:app_employee_management/components/component_appbar_popup.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/pages/page_attendance_date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageAttendanceSearch extends StatefulWidget {
   const PageAttendanceSearch({super.key, required this.dateStart, required this.dateEnd});

  final String dateStart;
  final String  dateEnd;

  @override
  State<PageAttendanceSearch> createState() => _PageAttendanceSearchState();
}

class _PageAttendanceSearchState extends State<PageAttendanceSearch> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
          title: '출퇴근 날짜 상세 검색'
      ),
      body: Center(
        child: _buildBody(),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          child: OutlinedButton(

            style: ButtonStyle(

            ),
            onPressed: () {
              String dateStart = DateFormat('yyyy-MM-dd').format(
                  _formKey.currentState!.fields['dateStart']!.value);
              String dateEnd = DateFormat('yyyy-MM-dd').format(
              _formKey.currentState!.fields['dateEnd']!.value);

                Navigator.pop(
                  context,
                  [true, dateStart, dateEnd],
                );
              },
            child: const Text('검색하기',
            style: TextStyle(
              fontFamily: 'naverTitle',
              color: colorMain
            ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        SizedBox(
          height: 20,
        ),
        FormBuilder(
          key: _formKey, // 글로벌키
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                FormBuilderDateTimePicker(
                name: 'dateStart',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                    color: colorMain
                  ),
                  labelText: '출퇴근 시작일',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),

                    onPressed: () {
                      _formKey.currentState!.fields['dateStart']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),
                SizedBox(
                  height: 30,
                ),
                FormBuilderDateTimePicker(
                name: 'dateEnd',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorMain
                  ),
                  labelText: '출퇴근 마지막일',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),

                    onPressed: () {
                      _formKey.currentState!.fields['dateEnd']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
