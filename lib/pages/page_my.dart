
import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/model/my_page_phone_update.dart';
import 'package:app_employee_management/pages/page_my_phone_update.dart';
import 'package:app_employee_management/pages/page_password.dart';
import 'package:app_employee_management/pages/page_withdrawal.dart';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:image_picker/image_picker.dart';
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/model/member_user_my_pageItem.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PageMy extends StatefulWidget {
  const PageMy({Key? key}) : super(key: key);

  @override
  State<PageMy> createState() => _PageMyState();
}

class _PageMyState extends State<PageMy> {

  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }

  final _scrollController = ScrollController();


  String password = '';
  String newPassword = '';
  String checkPassword = '';



  UserMyPageItem _myPage = UserMyPageItem('', '', '', '');

  String phone = '';


  @override
  void initState() {
    super.initState();
    _getMyPage();
  }





  Future<void> _getMyPage({bool reFresh = false}) async {
    if (reFresh) {
      UserMyPageItem('', '', '', '');
    }

      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().getMyPage().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _myPage = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });

    if (reFresh) {
      _scrollController.animateTo(
          0, duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: 'MyPage',
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          child:Column(
            children: [
              SizedBox(height:
              30,),
              Text('이름 - ${_myPage.name}',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 15,
                ),
              ),
              Divider(
                thickness: 3,
              ),
              SizedBox(height:
              40,),
                Row(

                  children: [
                    Spacer(flex: 1,),
                    Text('전화번호 - ${_myPage.phone}',
                      style: TextStyle(
                        fontFamily: 'maple',
                        fontSize: 15,
                      ),
                    ),
                   ElevatedButton(
                     style: ButtonStyle(
                       backgroundColor: MaterialStateProperty.all(colorMain),
                       minimumSize: MaterialStateProperty.all(Size(20, 40)),
                     ),
                     onPressed: () async {
                     final searchResult = await Navigator.push(
                         context,
                         MaterialPageRoute(
                             builder: (context) =>
                                 PageMyPhoneUpdate(
                                    )
                         )
                     ).then((res) => {
                       setState(() {
                         _getMyPage();
                       })
                     });

                     // if (searchResult != null && searchResult[0]) {
                     //   phone = searchResult[1];
                     //   _getMyPage(reFresh: true);
                     // }
                   }, child: Text('수정',
                   style: TextStyle(
                     fontFamily: 'maple',
                     color: Colors.white,
                   ),
                   ),
                   ),
                  ],
                ),

              Divider(
                thickness: 3,
              ),
              SizedBox(height:
              40,),
              Text('부서 - ${_myPage.department}',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 15,
                ),
              ),
              Divider(
                thickness: 3,
              ),
              SizedBox(height:
              40,),
              Text('직급 - ${_myPage.position}',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 15,
                ),
              ),
              Divider(
                thickness: 3,
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  SizedBox(width: 55,),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.red),
                      minimumSize: MaterialStateProperty.all(Size(20, 40)),
                    ),
                    onPressed: () async {
                      final searchResult = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  PageWithdrawal(
                                  )
                          )
                      );

                      if (searchResult != null && searchResult[0]) {
                        password = searchResult[1];
                        checkPassword = searchResult[2];
                        _getMyPage(reFresh: true);
                      }
                    },
                    child: Text('회원탈퇴',
                    style: TextStyle(
                      fontFamily: 'maple',
                      color: Colors.white
                    ),
                    ),
                  ),
                  SizedBox(width: 20,),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(colorMain),
                      minimumSize: MaterialStateProperty.all(Size(20, 40)),
                    ),
                    onPressed: () async {
                      final searchResult = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  PagePassword(
                                  )
                          )
                      );

                      if (searchResult != null && searchResult[0]) {
                        password = searchResult[1];
                        newPassword = searchResult[2];
                        checkPassword = searchResult[3];
                        _getMyPage(reFresh: true);
                      }
                    },
                    child: Text('비밀번호 변경',
                    style: TextStyle(
                      fontFamily: 'maple',
                      color: Colors.white
                    ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.white),
          ),
          onPressed: () {
            showDialog(context: context,
                barrierDismissible: true, //뒷배경의 touchEvent 가능여
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('로그아웃'), // 제목
                    content: const Text('정말 로그아웃 하시겠습니까?'), // 부제목
                    actions: [
                      OutlinedButton(
                          onPressed: () {
                            _logout(context);
                          },
                          child: const Text('확인')
                      ),
                      OutlinedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('취소')
                      ),
                    ],
                  );
                }
            );
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("로그아웃",
                style: TextStyle(
                  fontFamily: 'maple',
                  color: colorMain,
                  fontSize: 20,
                ),
              ),
              Icon(Icons.login,
                color: colorMain,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
