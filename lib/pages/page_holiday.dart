import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_dropdown.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/model/holiday_registration_request.dart';
import 'package:app_employee_management/model/member_login_request.dart';
import 'package:app_employee_management/model/member_login_response.dart';
import 'package:app_employee_management/model/member_login_response.dart';
import 'package:app_employee_management/pages/page_holiday_registration.dart';
import 'package:app_employee_management/pages/page_holiday_status.dart';
import 'package:app_employee_management/pages/page_holiday_use_status.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageHoliday extends StatefulWidget {
 const PageHoliday({Key? key}) : super(key: key);



  @override
  State<PageHoliday> createState() => _PageHolidayState();
}

class _PageHolidayState extends State<PageHoliday> {
  final _formKey = GlobalKey<FormBuilderState>();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '휴가관리',
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Center(
          child: Column(
            children: [
              Image.asset('assets/holiday.jpeg',
                width: 400,
                height: 150,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  fixedSize: MaterialStateProperty.all(Size.fromWidth(30)),
                  minimumSize: MaterialStateProperty.all(Size.fromHeight(60)),
                  backgroundColor: MaterialStateProperty.all(colorMain),
                    elevation: MaterialStateProperty.all(7)
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageHolidayRegistration(
                      ),
                    ),
                  );
                },
                child: Text('휴가신청',
                  style: TextStyle(
                    fontFamily: 'naverBold',
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  fixedSize: MaterialStateProperty.all(Size.fromWidth(30)),
                  minimumSize: MaterialStateProperty.all(Size.fromHeight(60)),
                    backgroundColor: MaterialStateProperty.all(colorMain),
                    elevation: MaterialStateProperty.all(7)
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageHolidayUseStatus(year: 0,
                      ),
                    ),
                  );
                },
                child: Text('휴가 사용현황',
                  style: TextStyle(
                    fontFamily: 'naverBold',
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  fixedSize: MaterialStateProperty.all(Size.fromWidth(30)),
                  minimumSize: MaterialStateProperty.all(Size.fromHeight(60)),
                    backgroundColor: MaterialStateProperty.all(colorMain),
                    elevation: MaterialStateProperty.all(7)
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageHolidayStatus(year: 0,
                      ),
                    ),
                  );
                },
                child: Text('휴가 신청현황',
                  style: TextStyle(
                    fontFamily: 'naverBold',
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        )
      ),
      backgroundColor: Colors.white,
    );
  }



}


