class HolidayUseItem{
  double countTotal;
  double countUse;
  double countRemaining;

  HolidayUseItem(this.countTotal, this.countUse, this.countRemaining);

  factory HolidayUseItem.fromJson(Map<String, dynamic> json) {
    return HolidayUseItem(
      json['countTotal'],
      json['countUse'],
      json['countRemaining'],
    );
  }
}