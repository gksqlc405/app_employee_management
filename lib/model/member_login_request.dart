class MemberLoginRequest {
  String username;
  String password;

  MemberLoginRequest(this.username, this.password);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;

    return data;
  }
}
