class AttendanceCount {
  int countResult;

  AttendanceCount(this.countResult);

  factory AttendanceCount.fromJson(Map<String, dynamic> json) {
    return AttendanceCount(
      json['countResult']
    );
  }
}