
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/model/holiday_status_item.dart';
import 'package:flutter/material.dart';

class ComponentHolidayStatusItem extends StatelessWidget {
  const ComponentHolidayStatusItem({super.key, required this.holidayStatusItem});

  final HolidayStatusItem holidayStatusItem;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: colorMain,
              blurRadius: 5.0,
              spreadRadius: 1.0,
            ),
          ],
          border: Border.all(
            color: Colors.white,
              style: BorderStyle.solid,
              width: 5,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Text(holidayStatusItem.holidayType,
            style: TextStyle(
              fontFamily: 'maple',
              fontSize: 15,
              color: colorMain
            ),
            ),
            Text(holidayStatusItem.dateHoliday,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            ),
            Text(holidayStatusItem.approvalStatus,
              style: TextStyle(
                fontFamily: 'naverBold',
                color: colorMain
              ),
            ),
            Text('등록 날짜 : ${holidayStatusItem.dateCreate}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
