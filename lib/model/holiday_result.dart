import 'package:app_employee_management/model/attendance_response.dart';
import 'package:app_employee_management/model/holiday_item.dart';

class HolidayResult {
  bool isSuccess;
  int code;
  String msg;
  HolidayUseItem date;

  HolidayResult(this.isSuccess, this.code, this.msg, this.date);

  factory HolidayResult.fromJson(Map<String, dynamic> json) {
    return HolidayResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      HolidayUseItem.fromJson(json['date']),
    );
  }
}