
class AttendanceItem {
  String attendanceStatus;
  String dateWork;
  String timeStart;
  String timeLeaveEarly;
  String timeLeave;
  AttendanceItem(this.attendanceStatus, this.dateWork, this.timeStart, this.timeLeaveEarly, this.timeLeave);

  factory AttendanceItem.fromJson(Map<String, dynamic> json) {
    return AttendanceItem(
      json['attendanceStatus'],
      json['dateWork'],
      json['timeStart'],
      json['timeLeaveEarly'],
      json['timeLeave'],
    );
  }
}

