class MemberLoginResponse {
  int memberId;

  MemberLoginResponse(
      this.memberId
      );

  factory MemberLoginResponse.fromJson(Map<String, dynamic> json) {
    return MemberLoginResponse(
        json['memberId']
    );
  }
}