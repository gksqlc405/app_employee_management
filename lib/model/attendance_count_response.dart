
  import 'attendance_count.dart';

class AttendanceCountResponse {
  AttendanceCount date;
  bool isSuccess;
  int code;
  String msg;

  AttendanceCountResponse(this.date, this.isSuccess, this.code, this.msg);

  factory AttendanceCountResponse.fromJson(Map<String, dynamic> json) {
  return AttendanceCountResponse(
    AttendanceCount.fromJson(json['date']),
  json['isSuccess'] as bool,
  json['code'],
  json['msg'],
  );
  }
  }

