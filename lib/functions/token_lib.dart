
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/pages/page_first.dart';
import 'package:app_employee_management/pages/page_member_login.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
/*
SharedPreferences 에 데이터가 저장되는 방식은 Map임을 반드시 숙지하고 갈 것.
Map이 무엇인지 알아야 함.
 */
class TokenLib {
  // token 가져오기
  static Future<String?> getToken() async {  // 메모리가 앱에 붙어있는게 아니라 Future 로 미래의 값을준다
    SharedPreferences prefs = await SharedPreferences.getInstance();  //메모리에 연결될때까지 기다린다.
    return prefs.getString('token');
  }


  // token 세팅
  static void setToken(String token) async {   // set 이기때문에 return 타입이 void
    SharedPreferences prefs = await SharedPreferences.getInstance(); //연결시도 계속해라 메모리 확인해야함
    prefs.setString('token', token); // 키값의 토큰에 값 토큰 저장해줘라
  }

  // 로그아웃 처리
  static void logout(BuildContext context) async {   // (로그아웃) 메모리에있는 저장되있는 토큰 키값 지우고 비회원용 메인페이지로 이동하기위해
    BotToast.closeAllLoading();  // 뭔가 남아있으면 안돼서 다 닫는다

    SharedPreferences prefs = await SharedPreferences.getInstance();  // 메모리에 접근해서 key 가 token인 값을 빈문자열('')로 바꿔버리고
    prefs.setString('token', '');  // 토큰을 지움 ''
    // 토스트 팝업 띄어주고
    ComponentNotification(
        success: false,
        title: '로그아웃',
        subTitle: '로그아웃되어 로그인 화면으로 이동합니다.'
    ).call();

// 지금까지 이동했던 모든 페이지 내역들 싹 다 지우고 page_index(비회원용 메인)으로 강제로 이동시킨다.
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMemberLogin()), (route) => false);   //pushAndRemoveUntil <- 지금까지 쌓여왔던것들 싹다 지우고 새로 하나 넣어라 (결국 하나만 남는다),
  }
}