class HolidayStatusItem {
  String holidayType;
  String dateHoliday;
  String approvalStatus;
  String dateCreate;

  HolidayStatusItem(this.holidayType, this.dateHoliday, this.approvalStatus, this.dateCreate);

  factory HolidayStatusItem.fromJson(Map<String, dynamic> json) {
    return HolidayStatusItem(
      json['holidayType'],
      json['dateHoliday'],
      json['approvalStatus'],
      json['dateCreate'],
    );
  }
}