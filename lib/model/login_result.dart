import 'package:app_employee_management/model/member_login_response.dart';

class LoginResult {
  MemberLoginResponse date;
  bool isSuccess;
  int code;
  String msg;

  LoginResult(
      this.date,
      this.isSuccess,
      this.code,
      this.msg
      );

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
        MemberLoginResponse.fromJson(json['date']),
      json['isSuccess'],
      json['code'],
      json['msg'],
    );
  }

}