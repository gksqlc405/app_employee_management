import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_no_contents.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_attendance_dropdown.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/config/config_holiday_dropdown.dart';
import 'package:app_employee_management/model/attendance_count.dart';
import 'package:app_employee_management/repository/repo_attendance.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageAttendanceCount extends StatefulWidget {
  const PageAttendanceCount({Key? key}) : super(key: key);

  @override
  State<PageAttendanceCount> createState() => _PageAttendanceCountState();
}

class _PageAttendanceCountState extends State<PageAttendanceCount> {
  int todayYear = DateTime.now().year;
  int todayMonth = DateTime.now().month;

  AttendanceCount _count = AttendanceCount(0);


  @override
  void initState() {
    super.initState();
    _getDetail();
  }


  Future<void> _getDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().getAttendanceCount(todayYear, todayMonth).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _count = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      // 이번에 추가된 알림창 컴포넌트. component_notification 참고
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      // 호출실패 시 창을 닫아버림. (데이터가 없는 시퀀스 혹은 인터넷 오류로 이 창을 유지시켜봤자
      // 의미가 없음.
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_count.countResult != 0) {
      return Scaffold(
        appBar: ComponentAppbarNormal(
          title: '근무일수',
        ),
        body: Container(
          child:
          Column(
            children: [
              SizedBox(height: 50,),
              _buildBody(),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(30),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: colorMain,
                      blurRadius: 1.0,
                      spreadRadius: 1.0,
                    ),
                  ],
                  border: Border.all(
                    color: colorMain,
                    style: BorderStyle.solid,
                  ),
                  borderRadius: BorderRadius.circular(60.0),
                ),
                child: Text('$todayYear년 $todayMonth월에 \n\n 총 ${_count.countResult}일 출근하셨습니다',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'maple',
                    fontSize: 15,
                  ),
                ),
              ),
              Image.asset('assets/workday.jpeg',
              width: 150,
                height: 150,
              ),
            ],
          ),
        ),
        backgroundColor: Colors.white,
      );
    } else {
      return Scaffold(
        appBar: ComponentAppbarNormal(
          title: '출근일수',
        ),
        body:  SizedBox(
          height: MediaQuery.of(context).size.height - 40 - 20,
          child: const ComponentNoContents(imageName: 'line.png', msg: '근무일수 정보가 없습니다.'),
    ),
      );
    }
  }

  Widget _buildBody() {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          FormBuilderDropdown<int>(
            name: 'todayYear',
            decoration: const InputDecoration(
              labelText: '출근일수 년도',
              labelStyle: TextStyle(
                  fontFamily: 'maple',
                color: colorMain,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(60)),
                borderSide: BorderSide(color: colorMain),
              ),
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
            ]),
            items: dropdownHolidayUse,
            initialValue: todayYear,
            onChanged: (val) {
              setState(() {
                todayYear = val!;
              });
              _getDetail();
            },
          ),
          SizedBox(
            height: 30,
          ),
          FormBuilderDropdown<int>(
            name: 'todayMonth',
            decoration: const InputDecoration(
              labelText: '출근일수 월',
              labelStyle: TextStyle(
                  fontFamily: 'maple',
                color: colorMain,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(60)),
                borderSide: BorderSide(color: colorMain),
              ),
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
            ]),
            items: dropdownAttendance,
            initialValue: todayMonth,
            onChanged: (val) {
              setState(() {
                todayMonth = val!;
              });
              _getDetail();
            },
          ),
        ],
      ),
    );
  }
}
