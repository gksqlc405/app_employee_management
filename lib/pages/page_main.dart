import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/pages/page_attendance.dart';
import 'package:app_employee_management/pages/page_holiday.dart';
import 'package:app_employee_management/pages/page_my.dart';
import 'package:flutter/material.dart';

class PageMain extends StatefulWidget {
  const PageMain({Key? key}) : super(key: key);

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  int _currentTabIndex = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    final _kTabPages = <Widget>[
      // const Center(child: PageHome(),),
      const Center(child: PageAttendance()),
       Center(child: PageHoliday()),
      const Center(child: PageMy()),
    ];
    final _kBottmonNavBarItems = <BottomNavigationBarItem>[
      // const BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
      const BottomNavigationBarItem(icon: Icon(Icons.hail,color: colorMain),
          label: '근태관리',),
      const BottomNavigationBarItem(icon: Icon(Icons.favorite_border,color: colorMain), label: '휴가관리'),
      const BottomNavigationBarItem(icon: Icon(Icons.person_pin,color: colorMain), label: 'MyPage'),
    ];
    assert(_kTabPages.length == _kBottmonNavBarItems.length);
    final bottomNavBar = BottomNavigationBar(
      selectedItemColor: colorMain,
      items: _kBottmonNavBarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentTabIndex = index;
        });
      },
    );
    return Scaffold(
      body: _kTabPages[_currentTabIndex],
      bottomNavigationBar: bottomNavBar,
    );
  }
}
