import 'package:flutter/material.dart';

const List<DropdownMenuItem<String>> dropdownHolidayType = [
  DropdownMenuItem(value: 'ANNUAL_LEAVE', child: Text('연차')),
  DropdownMenuItem(value: 'HALF_PM_LEAVE', child: Text('오후반차')),
  DropdownMenuItem(value: 'HALF_AM_LEAVE', child: Text('오전반차')),
  DropdownMenuItem(value: 'SICK_LEAVE', child: Text('병가')),
];


