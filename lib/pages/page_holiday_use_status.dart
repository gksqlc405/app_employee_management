
import 'package:app_employee_management/components/component_appbar_actions.dart';
import 'package:app_employee_management/components/component_appbar_filter.dart';
import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_appbar_popup.dart';
import 'package:app_employee_management/components/component_count_title.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_holiday_item.dart';
import 'package:app_employee_management/components/component_no_contents.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/config/config_holiday_dropdown.dart';
import 'package:app_employee_management/model/holiday_item.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
// todo:
class PageHolidayUseStatus extends StatefulWidget {
   PageHolidayUseStatus({super.key, required this.year});

   int year;

  @override
  State<PageHolidayUseStatus> createState() => _PageHolidayUseStatusState();
}

class _PageHolidayUseStatusState extends State<PageHolidayUseStatus> {


  HolidayUseItem _holidayUse = HolidayUseItem(0, 0, 0);

  int todayYear = DateTime.now().year;


  @override
  void initState() {
    super.initState();
    _getHolidayUse();
  }

  Future<void> _getHolidayUse() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().getHolidayUse(todayYear).then((res) {
      BotToast.closeAllLoading();


      setState(() {
        _holidayUse = res.date;


      });
    }).catchError((err) {
      BotToast.closeAllLoading();


      // 이번에 추가된 알림창 컴포넌트. component_notification 참고
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '$todayYear년도에 사용한 연차가없습니다',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    if ( _holidayUse.countTotal != 0) {
      return Scaffold(
        appBar: const ComponentAppbarNormal(
            title: '휴가사용 날짜 검색'
        ),
        body:
        Container(
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              _buildBody(),
              SizedBox(height: 20,),
              Text('총 휴가일수가 ${_holidayUse.countTotal} 개 입니다.',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 20,
                ),
              ),
              SizedBox(height: 20,),
              Text('사용 휴가일수가 ${_holidayUse.countUse} 개 입니다',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 20,
                ),
              ),
              SizedBox(height: 20,),
              Text('휴가 잔여일수가 : ${_holidayUse.countRemaining} 개 남았습니다',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 20,
                ),
              ),
              Image.asset('assets/count.jpeg',
                height: 300,
              ),
            ],
          ),
        ),
        backgroundColor: Colors.white,
      );
    }else {
      return Scaffold(
        appBar: ComponentAppbarNormal(
          title: '휴가사용 날짜 검색',
        ),
        body:  SizedBox(
          height: MediaQuery.of(context).size.height - 40 - 20,
          child: const ComponentNoContents(imageName: 'line.png', msg: '휴가일수가 없습니다.'),
        ),
      );
    }
    }


  Widget _buildBody() {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          FormBuilderDropdown<int>(
            name: 'todayYear',
            decoration: const InputDecoration(
              labelText: '휴가 사용 년도',
              labelStyle: TextStyle(
                  fontFamily: 'maple',
                  color: colorMain
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                borderSide: BorderSide(color: colorMain),
              ),
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
            ]),
            items: dropdownHolidayUse,
            initialValue: todayYear,
            onChanged: (val) {
              setState(() {
                todayYear = val!;
              });
              _getHolidayUse();
            },
          ),

        ],
      ),
    );
  }
}



