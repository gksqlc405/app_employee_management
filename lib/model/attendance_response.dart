class AttendanceResponse {
   String attendanceStatusName;
   String attendanceStatus;

   AttendanceResponse(
       this.attendanceStatusName, this.attendanceStatus
       );

   factory AttendanceResponse.fromJson(Map<String, dynamic> json) {
     return AttendanceResponse(
       json['attendanceStatusName'],
       json['attendanceStatus']
     );
   }
}