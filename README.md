# 사원관리 APP

***

#### LANGUAGE
```
Dart 
Flutter
```
#### 기능
```

* 로그인 관리
  - 사용자 로그인 (Post)
  - 비밀번호 변경 (Put)
  
* 근태관리(사용자)
  - 근무일수 확인 (Get)
  - 출퇴근 날짜 확인 (Get)
  - 근태 상태 변경 (Put)
  - 근태 상태 가저오기 (Get)
 
  * 휴가 신청(사용자)
  - 휴가 신청 현황 (Get)
  - 휴가 사용 현황 (Get)
  - 휴가 신청 (Post)
  
* 사원정보(사용자)
  - 사원 탈퇴 (Del)
  - MyPage 가저오기 (Get)
  - 회원 폰번호 수정 (Put)
  


```


# app 화면

### 로그인 화면
![login](./images/login.png)

### 근태관리 화면
![Attendance](./images/attendance.png)

### 출근 버튼 화면
![Attendance](./images/attendance1.png)

### 조퇴 퇴근 버튼 및 결과
![Attendance](./images/attendance2.png)

### 출퇴근 날짜 상세 검색
![Attendance](./images/attendance3.png)

### 출퇴근 날짜 검색 결과
![Attendance](./images/attendance4.png)

### 날짜필터로 근무일수 보여주기
![Attendance](./images/attendance5.png)

### 휴가관리
![Holiday](./images/holiday.png)

### 휴가 신청
![Holiday](./images/holiday1.png)

### 사용한 휴가 날짜필터로 검색해 갯수 보여주기
![Holiday](./images/holiday2.png)

### 휴가 신청한 내역들을 날짜필터로 원하는 날짜에 보여주기
![Holiday](./images/holiday3.png)

### MyPage
![MyPage](./images/mypage.png)

### 전화번호 수정
![MyPage](./images/mypage1.png)

### 회원 탈퇴
![MyPage](./images/mypage2.png)

### 비밀번호 변경
![MyPage](./images/mypage3.png)

### 로그아웃
![MyPage](./images/mypage4.png)
















