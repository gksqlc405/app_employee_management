import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/middleware/middleware_login_check.dart';
import 'package:app_employee_management/model/member_login_request.dart';
import 'package:app_employee_management/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageMemberLogin extends StatefulWidget {
  const PageMemberLogin({Key? key}) : super(key: key);

  @override
  State<PageMemberLogin> createState() => _PageMemberLoginState();
}

class _PageMemberLoginState extends State<PageMemberLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _memberLogin(MemberLoginRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().memberLogin(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();
      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setToken(res.date.memberId.toString());

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);


    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '로그인',
      ),
      body:
      SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 60,),
              _buildBody(),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,


    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
            padding: EdgeInsets.all(15),
            margin: EdgeInsets.all(15),
            child: Column(
              children: [
                FormBuilderTextField(
                  name: 'username',
                  decoration: const InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: colorMain)
                      ),
                      labelText: '아이디를 입력해 주세요.',
                      labelStyle: TextStyle(
                        color: Colors.grey,
                          fontFamily: 'navertitle'
                      ),

                      suffixIcon: Icon(Icons.lock,color: colorMain,)

                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                    FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                    FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                  ]),
                  keyboardType: TextInputType.text,
                ),
                SizedBox(
                  height: 30,
                ),
                FormBuilderTextField(
                  name: 'password',
                  decoration: const InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: colorMain)
                      ),
                      labelText: '비밀번호를 입력해 주세요.',
                      labelStyle: TextStyle(
                          color: Colors.grey,
                          fontFamily: 'navertitle'
                      ),
                      suffixIcon: Icon(Icons.key,color: colorMain,)

                  ),
                  obscureText: true,// 비밀번호 안보이게하기
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                    FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                    FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                  ]),
                  keyboardType: TextInputType.text,
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(colorMain),
                    fixedSize: MaterialStateProperty.all(Size.fromWidth(300)),
                    minimumSize: MaterialStateProperty.all(Size.fromHeight(60)),
                    textStyle: MaterialStateProperty.all(TextStyle(fontFamily: 'naverTitle',color: Colors.white)),

                  ),
                    onPressed: () {
                      //사용자가 가입 버튼을 클릭하면 을 호출 _formKey.currentState.validate()합니다. validator에 정의된 모든 함수 를 호출 합니다.
                      //validator함수 는 어떤 종류의 오류를 알리기 위해 String을 반환해야 하며, 오류가 없으면 null을 반환해야 합니다 .
                      if (_formKey.currentState?.saveAndValidate() ?? false) { // ?? 연산자는 좌항이 nll이 아니면 좌항값을 리턴하고 null이면 우항값을 리턴한다
                        MemberLoginRequest loginRequest = MemberLoginRequest(
                          _formKey.currentState!.fields['username']!.value,
                          _formKey.currentState!.fields['password']!.value,
                        );
                        _memberLogin(loginRequest);
                      }
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text("로그인",
                        style: TextStyle(
                          color: Colors.white
                        ),),
                        Icon(Icons.login,
                        color: Colors.white,
                        ),

                      ],
                    ),
                ),
                Image.asset('assets/friends.png',
                width: 300,
                  height: 250,
                ),
              ],
            ),
          ),
      ),
    );
  }
}
