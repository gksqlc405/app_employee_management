
import 'package:app_employee_management/pages/page_member_login.dart';
import 'package:flutter/material.dart';

class PageFirst extends StatelessWidget {
  const PageFirst({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset('assets/태연.png'),
            SizedBox(
              height: 50,
            ),
            Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageMemberLogin())
                    );
                  }, child: Text(
                    '로그인'
                ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
