import 'package:app_employee_management/model/attendance_response.dart';

class AttendanceResult {
  bool isSuccess;
  int code;
  String msg;
  AttendanceResponse date;

  AttendanceResult(this.isSuccess, this.code, this.msg, this.date);

  factory AttendanceResult.fromJson(Map<String, dynamic> json) {
    return AttendanceResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
     AttendanceResponse.fromJson(json['date']),
    );
  }
}