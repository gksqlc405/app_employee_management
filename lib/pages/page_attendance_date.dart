import 'dart:core';
import 'package:app_employee_management/components/component_appbar_filter.dart';
import 'package:app_employee_management/components/component_appbar_normal.dart';
import 'package:app_employee_management/components/component_attendance_item.dart';
import 'package:app_employee_management/components/component_count_title.dart';
import 'package:app_employee_management/components/component_custom_loading.dart';
import 'package:app_employee_management/components/component_list_item.dart';
import 'package:app_employee_management/components/component_no_contents.dart';
import 'package:app_employee_management/components/component_notification.dart';
import 'package:app_employee_management/config/config_color.dart';
import 'package:app_employee_management/config/config_form_validator.dart';
import 'package:app_employee_management/model/attendance_item.dart';
import 'package:app_employee_management/pages/page_attendance_search.dart';
import 'package:intl/intl.dart';
import 'package:app_employee_management/repository/repo_attendance.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageAttendanceDate extends StatefulWidget {
  const PageAttendanceDate({Key? key}) : super(key: key);

  @override
  State<PageAttendanceDate> createState() => _PageAttendanceDateState();
}

class _PageAttendanceDateState extends State<PageAttendanceDate> with AutomaticKeepAliveClientMixin {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
  final _scrollController = ScrollController();



  List<AttendanceItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  String _dateStart =  DateFormat('yyyy-MM-dd').format(DateTime.now());
  String _dateEnd = DateFormat('yyyy-MM-dd').format(DateTime.now());



  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) { // 시작하기전에 미리 띄우고
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });


      await RepoAttendance()
          .getList(dateStart: _dateStart, dateEnd: _dateEnd)
          .then((res) {
        BotToast.closeAllLoading(); // 성공했을때도 닫고

        setState(() {
          _totalPage = res.totalPage;
          _totalItemCount = res.totalItemCount;


          _list = [..._list, ...res.list];

          _currentPage++;
        });
      }).catchError((err) => BotToast.closeAllLoading()); // 실패했을때도 닫어라

    }

    if (reFresh) {
      _scrollController.animateTo(
          0, duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarFilter(
        title: '출퇴근 날짜 조회',
        actionIcon: Icons.search,
        callback: () async {
          final searchResult = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      PageAttendanceSearch(
                          dateStart: _dateStart, dateEnd: _dateEnd)
              )
          );

          if (searchResult != null && searchResult[0]) {
            _dateStart = searchResult[1];
            _dateEnd = searchResult[2];
            _loadItems(reFresh: true);
          }
        },
      ),
      body: Center(
        child: ListView(
          controller: _scrollController,
          children: [
            SizedBox(
              height: 30,
            ),
            ComponentCountTitle(icon: Icons.work_history_outlined,
                count: _totalItemCount,
                unitName: '건',
                itemName: '출퇴근 정보'
            ),
            Divider(
              thickness: 1,
              color: colorMain,
            ),

              Image.asset('assets/date.jpeg',
                width: 200,
                height: 200,
              ),

            _buildBody(),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }


  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Center(
        child:
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentListItem(item: _list[index]),
            )
          ],
        ),
      );
    } else {
      return SizedBox(

        height: MediaQuery.of(context).size.height - 200 - 150,
        child: const ComponentNoContents(imageName: 'line.png', msg: '출퇴근 날짜 정보가 없습니다.'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}