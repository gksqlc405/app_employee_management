import 'package:app_employee_management/config/config_api.dart';
import 'package:app_employee_management/functions/token_lib.dart';
import 'package:app_employee_management/model/common_result.dart';
import 'package:app_employee_management/model/holiday_list_result.dart';
import 'package:app_employee_management/model/holiday_result.dart';
import 'package:app_employee_management/model/holiday_registration_request.dart';
import 'package:app_employee_management/model/login_result.dart';
import 'package:app_employee_management/model/member_login_request.dart';
import 'package:app_employee_management/model/member_result.dart';
import 'package:app_employee_management/model/member_user_my_pageItem.dart';
import 'package:app_employee_management/model/my_page_phone_update.dart';
import 'package:app_employee_management/model/password_update_request.dart';
import 'package:app_employee_management/model/user_withdrawal_request.dart';
import 'package:dio/dio.dart';

class RepoMember{



  Future<LoginResult> memberLogin(MemberLoginRequest request) async {
    const String baseUrl = '$apiUrl/login/memberLogin';
    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return LoginResult.fromJson(response.data);
  }
  Future<HolidayLisResult> getHolidayList(int year) async {
    const String baseUrl = '$apiUrl/approval/holidayStatus/{memberId}/year/{year}';


    Dio dio = Dio();
    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!).replaceAll('{year}', year.toString()),
        options: Options(
          followRedirects: false,
        )
    );

    return HolidayLisResult.fromJson(response.data);
  }


  Future<CommonResult> setHolidayRegistration(HolidayRegistrationRequest request) async {
    const String baseUrl = '$apiUrl/approval/post/{memberId}';
    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.post(
        baseUrl.replaceAll('{memberId}', token!),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }



  Future<HolidayResult> getHolidayUse(int year) async {
    const String baseUrl = '$apiUrl/approval/holidayUse/member-id/{memberId}/year/{year}';
    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!).replaceAll('{year}', year.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return HolidayResult.fromJson(response.data);
  }


  Future<MemberResult> getMyPage() async {
    const String baseUrl = '$apiUrl/user/myPage/{memberId}';
    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return MemberResult.fromJson(response.data);
  }


  Future<CommonResult> putPhoneUpdate(MyPagePhoneUpdate myPagePhoneUpdate) async {
    const String baseUrl = '$apiUrl//user/phone/{memberId}';

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.put(
      baseUrl.replaceAll('{memberId}', token!),
      data: myPagePhoneUpdate.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      ),
    );
    return CommonResult.fromJson(response.data);
  }


  Future<CommonResult> delMember(UserWithdrawalRequest request) async {
    const String baseUrl = '$apiUrl/user/member/{memberId}';
    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.delete(
      baseUrl.replaceAll('{memberId}', token!),
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      ),
    );

    return CommonResult.fromJson(response.data);
  }



  Future<CommonResult> putPassword(PasswordUpdateRequest request) async {
    const String baseUrl = '$apiUrl//login/password/{memberId}';

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.put(
      baseUrl.replaceAll('{memberId}', token!),
      data: request.toJson(),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );
    return CommonResult.fromJson(response.data);
  }
}