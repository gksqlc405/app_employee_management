
import 'package:app_employee_management/model/attendance_item.dart';
import 'package:flutter/material.dart';

class ComponentAttendanceItem extends StatelessWidget {
  const ComponentAttendanceItem({super.key, required this.attendanceItem});

  final AttendanceItem attendanceItem;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
              spreadRadius: 3.0,
            ),
          ],
          border: Border.all(
            color: Colors.blueAccent,
            style: BorderStyle.solid,
            width: 5,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Text(attendanceItem.attendanceStatus,
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 15,
              ),
            ),
            Text('출근일 : ${attendanceItem.dateWork}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),

    );
  }
}
